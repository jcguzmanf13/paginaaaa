$("a[href^='#']").click(function(event){
	event.preventDefault()
	var href = $(this).attr('href') //valor igual a #one
	var position = $(href).offset()

	$('body,html').animate({
		scrollTop: position.top
	}, 600)
})
//-----------------------------------------------------------------------------------------------------------------

var $body = $('<div class="container" />').appendTo('#cuatro');
var $results = $('<div id="root" />').appendTo($body);
    
var request = new XMLHttpRequest()
request.open('GET', 'https://jsonplaceholder.typicode.com/users', true)
request.onload = function() {
    

    var respuesta = JSON.parse(this.response)
    var root = document.getElementById('root')
    

    
    if(this.status >= 200 && this.status < 400) {

        respuesta.forEach(function(objeto, indice) {
            var $cardImg = $('<div class="card mb-3 "/>').appendTo($results)
            var $rowNo = $('<div class="row no-gutters"/>').appendTo($cardImg)
            var $tamano = $('<div class="col-md-4"/>').appendTo($rowNo)

            for(i = 0; i <= respuesta.length; i++ ){
                var aNumeros = new Array (1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);
                var random = Math.floor(Math.random() * aNumeros.length);
                var $fotoUrl = 'https://picsum.photos/id/1'+random+'/1000'
            }

            var $fotito = $('<img src="'+$fotoUrl+'" class="card-img" alt="..."/>').appendTo($tamano)

            var $segundoDiv = $('<div class="-col-md-8" />').appendTo($rowNo)
            var $cardBody = $('<div class="card-body" />').appendTo($segundoDiv)
            var $nombre = $('<h5 class="card-title"> '+objeto.name+'</h5>').appendTo($cardBody)
            var $primerP = $('<p class="card-text">' + objeto.company.name + '<p/>').appendTo($cardBody)
            var $tercerP = $('<p class="card-text">' + objeto.company.catchPhrase + '<p/>').appendTo($cardBody)
            var $cuartoP = $('<p class="card-text">' + objeto.company.bs + '<p/>').appendTo($cardBody)
            var $quintoP = $('<p class="card-text">' + objeto.email + '<p/>').appendTo($cardBody)
            var $segundoP = $('<p class="card-text">'+'<small class="text-muted"> ' +'Last updated '+random +' mins ago. </small><p/>').appendTo($cardBody)

        })
    }

}
request.send()

//Get the button:
mybutton = document.getElementById("myBtn");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

$(document).ready(function(){
    $('.toggle').click(function(){
        $('.toggle').toggleClass('active')
        $('#dos').toggleClass('night')
        $('#cuatro').toggleClass('night')
        $('footer').toggleClass('night')
        $('body').toggleClass('night')
        $('nav').toggleClass('night')
        $('div.card').toggleClass('naigth')
    })
})


var TxtType = function(el, toRotate, period) {
    this.toRotate = toRotate;
    this.el = el;
    this.loopNum = 0;
    this.period = parseInt(period, 10) || 2000;
    this.txt = '';
    this.tick();
    this.isDeleting = false;
};

TxtType.prototype.tick = function() {
 
    var i = this.loopNum % this.toRotate.length;
    var fullTxt = this.toRotate[i];

    if (this.isDeleting) {
    this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
    this.txt = fullTxt.substring(0, this.txt.length + 1);
    }

    this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

    var that = this;
    var delta = 200 - Math.random() * 100;

    if (this.isDeleting) { delta /= 2; }

    if (!this.isDeleting && this.txt === fullTxt) {
    delta = this.period;
    this.isDeleting = true;
    } else if (this.isDeleting && this.txt === '') {
    this.isDeleting = false;
    this.loopNum++;
    delta = 500;
    }

    setTimeout(function() {
    that.tick();
    }, delta);
};

window.onload = function() {
    var elements = document.getElementsByClassName('typewrite');
    for (var i=0; i<elements.length; i++) {
        var toRotate = elements[i].getAttribute('data-type');
        var period = elements[i].getAttribute('data-period');
        if (toRotate) {
          new TxtType(elements[i], JSON.parse(toRotate), period);
        }
    }
};